#ifndef STREAM9_FILESYSTEM_JSON_IPP
#define STREAM9_FILESYSTEM_JSON_IPP

#include <stream9/filesystem/json.hpp>

#include <stream9/filesystem/symbol.hpp>

#include <iostream>

#include <stream9/json.hpp>

namespace stream9::json {

template<typename CharT, typename Traits>
void
tag_invoke(json::value_from_tag, json::value& v,
           std::basic_iostream<CharT, Traits>& s)
{
    auto& obj = v.emplace_object();
    auto const state = s.rdstate();

    obj["rdstate"] = stream9::filesystem::iostate_to_symbol(state);
    obj["input position"] = static_cast<std::streamoff>(s.tellg());
    obj["output position"] = static_cast<std::streamoff>(s.tellp());

    obj["fmtflags"] = stream9::filesystem::fmtflags_to_symbol(s.flags());
    obj["precision"] = s.precision();
    obj["width"] = s.width();
    obj["locale"] = s.getloc().name();

    if (auto const e = s.exceptions(); e != std::ios_base::goodbit) {
        obj["exceptions"] = stream9::filesystem::iostate_to_symbol(e);
    }

    s.clear(state);
}

template<typename CharT, typename Traits>
void
tag_invoke(json::value_from_tag, json::value& v,
           std::basic_istream<CharT, Traits>& s)
{
    auto& obj = v.emplace_object();
    auto const state = s.rdstate();

    obj["rdstate"] = stream9::filesystem::iostate_to_symbol(state);
    obj["input position"] = static_cast<std::streamoff>(s.tellg());

    obj["fmtflags"] = stream9::filesystem::fmtflags_to_symbol(s.flags());
    obj["precision"] = s.precision();
    obj["width"] = s.width();
    obj["locale"] = s.getloc().name();

    if (auto const e = s.exceptions(); e != std::ios_base::goodbit) {
        obj["exceptions"] = stream9::filesystem::iostate_to_symbol(e);
    }

    s.clear(state);
}

template<typename CharT, typename Traits>
void
tag_invoke(json::value_from_tag, json::value& v,
           std::basic_ostream<CharT, Traits>& s)
{
    auto& obj = v.emplace_object();
    auto const state = s.rdstate();

    obj["rdstate"] = stream9::filesystem::iostate_to_symbol(state);
    obj["output position"] = static_cast<std::streamoff>(s.tellp());

    obj["fmtflags"] = stream9::filesystem::fmtflags_to_symbol(s.flags());
    obj["precision"] = s.precision();
    obj["width"] = s.width();
    obj["locale"] = s.getloc().name();

    if (auto const e = s.exceptions(); e != std::ios_base::goodbit) {
        obj["exceptions"] = stream9::filesystem::iostate_to_symbol(e);
    }

    s.clear(state);
}

} // namespace stream9::json

#endif // STREAM9_FILESYSTEM_JSON_IPP
