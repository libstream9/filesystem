#ifndef STREAM9_FILESYSTEM_LOAD_FILE_HPP
#define STREAM9_FILESYSTEM_LOAD_FILE_HPP

#include "error.hpp"

#include <stream9/array.hpp>
#include <stream9/cstring_ptr.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::filesystem {

void
load_file(cstring_ptr const& path,
          array<char>& buffer,
          safe_integer<int64_t, 0> size_limit = 8 * 1024 * 1024);

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_LOAD_FILE_HPP
