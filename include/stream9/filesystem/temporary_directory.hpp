#ifndef STREAM9_FILESYSTEM_TEMPORARY_DIRECTORY_HPP
#define STREAM9_FILESYSTEM_TEMPORARY_DIRECTORY_HPP

#include <compare>
#include <ostream>

#include <stream9/cstring_ptr.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/optional.hpp>
#include <stream9/path/concat.hpp>
#include <stream9/string.hpp>

namespace stream9::filesystem {

/**
 * @model std::default_initiailzable
 * @model std::movable
 * @model std::destructible
 * @model std::totally_ordered
 */
class temporary_directory
{
public:
    /**
     * Create temporary directory
     *
     * A directory will be uniquely named with prefix and random characters
     * (see mkdtemp(3)).
     * If prefix is empty or relative path, temorary directory will be created
     * under fs::temp_directory_path().
     * If any parent of prefix doesn't exist they will be created.
     *
     * @param prefix prefix path of temporary directory
     * @throw fs::filesystem_error
     */
    temporary_directory(opt<cstring_ptr> const& prefix = {});

    /**
     * On destruction, temporary directory and all of its contents will be
     * deleted from filesystem. Each parent of temporary directory which is
     * created by constructor will be deleted if it doesn't contain any file
     * other than directory created by this class.
     */
    ~temporary_directory() noexcept;

    temporary_directory(temporary_directory&&) = default;
    temporary_directory& operator=(temporary_directory&&) = default;

    // query

    /**
     * @return absolute path to temporary directory
     */
    cstring_view path() const { return m_path; }

    bool auto_delete() const noexcept { return m_auto_delete; }

    // modifier
    void set_auto_delete(bool v) noexcept { m_auto_delete = v; }

    // comparison
    bool operator==(temporary_directory const&) const = default;
    std::strong_ordering operator<=>(temporary_directory const&) const;

    // conversion
    operator cstring_view () const noexcept { return path(); }

private:
    string m_path;
    size_t m_delete_depth;
    bool m_auto_delete = true;
};

template<str::string S1>
string
operator/(temporary_directory const& p1, S1&& p2)
{
    return stream9::path::concat(p1.path(), p2);
}

inline std::strong_ordering temporary_directory::
operator<=>(temporary_directory const& other) const
{
    return m_path <=> other.m_path;
}

inline std::ostream&
operator<<(std::ostream& os, temporary_directory const& t)
{
    return os << t.path();
}

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_TEMPORARY_DIRECTORY_HPP
