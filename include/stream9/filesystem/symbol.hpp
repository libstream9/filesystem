#ifndef STREAM9_FILESYSTEM_SYMBOL_HPP
#define STREAM9_FILESYSTEM_SYMBOL_HPP

#include <ios>
#include <string>

namespace stream9::filesystem {

std::string
iostate_to_symbol(std::ios_base::iostate);

std::string
fmtflags_to_symbol(std::ios_base::fmtflags);

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_SYMBOL_HPP
