#ifndef STREAM9_FILESYSTEM_EXISTS_HPP
#define STREAM9_FILESYSTEM_EXISTS_HPP

#include "namespace.hpp"

#include <stream9/cstring_ptr.hpp>

namespace stream9::filesystem {

bool exists(cstring_ptr const& path);

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_EXISTS_HPP
