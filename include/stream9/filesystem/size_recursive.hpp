#ifndef STREAM9_FILESYSTEM_SIZE_HPP
#define STREAM9_FILESYSTEM_SIZE_HPP

#include "error.hpp"
#include "namespace.hpp"

#include <cstdint>

#include <stream9/array.hpp>
#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/optional.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::filesystem {

using std::intmax_t;

//TODO return errors during scanning

/*
 * Compute the size of file/directory recursively.
 * - If error happens during the scanning, then see the size of
 *   a file that caused error as 0 and continue scanning.
 * - Symbolic links will not be followed (to avoid checking circular links).
 */
safe_integer<intmax_t, 0>
size_recursive(cstring_ptr const& pathname);

safe_integer<intmax_t, 0>
size_recursive(lx::fd_ref dir,
               cstring_ptr const& pathname);

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_SIZE_HPP
