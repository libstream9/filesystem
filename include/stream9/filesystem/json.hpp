#ifndef STREAM9_FILESYSTEM_JSON_HPP
#define STREAM9_FILESYSTEM_JSON_HPP

#include <iosfwd>

namespace stream9::json {

struct value_from_tag;
class value;

template<typename CharT, typename Traits>
void
tag_invoke(json::value_from_tag, json::value&,
           std::basic_istream<CharT, Traits>&);

extern template void
tag_invoke(json::value_from_tag, json::value&, std::basic_istream<char>&);

template<typename CharT, typename Traits>
void
tag_invoke(json::value_from_tag, json::value&,
           std::basic_ostream<CharT, Traits>&);

extern template void
tag_invoke(json::value_from_tag, json::value&, std::basic_ostream<char>&);

template<typename CharT, typename Traits>
void
tag_invoke(json::value_from_tag, json::value&,
           std::basic_iostream<CharT, Traits>&);

extern template void
tag_invoke(json::value_from_tag, json::value&, std::basic_iostream<char>&);

} // namespace stream9::json

#endif // STREAM9_FILESYSTEM_JSON_HPP
