#ifndef STREAM9_FILESYSTEM_MKDIR_RECURSIVE_HPP
#define STREAM9_FILESYSTEM_MKDIR_RECURSIVE_HPP

#include "error.hpp"

#include <sys/stat.h> // mode_t

#include <stream9/cstring_ptr.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::filesystem {

safe_integer<int, 0>
mkdir_recursive(cstring_ptr const& path, ::mode_t mode = 0777);

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_MKDIR_RECURSIVE_HPP
