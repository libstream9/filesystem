#ifndef STREAM9_FILESYSTEM_TMPFSTREAM_HPP
#define STREAM9_FILESYSTEM_TMPFSTREAM_HPP

#include "error.hpp"
#include "namespace.hpp"
#include "temporary_file.hpp"

#include <stream9/string.hpp>

#include <ext/stdio_filebuf.h>

namespace stream9::filesystem {

class tmpfstream
    : public std::basic_iostream<char>
{
    using base_t = std::basic_iostream<char>;

public:
    tmpfstream(string_view prefix = "") noexcept
        : m_file { prefix }
        , m_rdbuf { m_file.fd().release(), std::ios_base::in | std::ios_base::out }
    {
        this->init(&m_rdbuf);
    }

    tmpfstream(tmpfstream const&) = delete;
    tmpfstream& operator=(tmpfstream const&) = delete;

    tmpfstream(tmpfstream&& other) noexcept
        : base_t(std::move(other))
        , m_file { std::move(other.m_file) }
        , m_rdbuf { std::move(other.m_rdbuf) }
    {
        this->init(&m_rdbuf);
    }

    tmpfstream& operator=(tmpfstream&& other) noexcept
    {
        base_t::operator=(std::move(other));
        m_file = std::move(other.m_file);
        m_rdbuf = std::move(other.m_rdbuf);

        return *this;
    }

    ~tmpfstream() noexcept = default;

    // query
    auto path() const noexcept { return m_file.path(); }
    bool auto_delete() const noexcept { return m_file.auto_delete(); }

    // modifier
    void set_auto_delete(bool v) noexcept { m_file.set_auto_delete(v); }

    // command
    void close();

    void rename(string_view newpath);

private:
    temporary_file m_file;
    __gnu_cxx::stdio_filebuf<char> m_rdbuf;
};

inline void tmpfstream::
close()
{
    try {
        m_file.close();
    }
    catch (...) {
        rethrow_error();
    }
}

inline void tmpfstream::
rename(string_view newpath)
{
    try {
        m_file.rename(newpath);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_TMPFSTREAM_HPP
