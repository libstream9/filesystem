#ifndef STREAM9_FILESYSTEM_IS_DIRECTORY_HPP
#define STREAM9_FILESYSTEM_IS_DIRECTORY_HPP

#include <stream9/cstring_ptr.hpp>

namespace stream9::filesystem {

bool is_directory(cstring_ptr path) noexcept;

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_IS_DIRECTORY_HPP
