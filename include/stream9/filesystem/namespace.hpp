#ifndef STREAM9_FILESYSTEM_NAMESPACE_HPP
#define STREAM9_FILESYSTEM_NAMESPACE_HPP

namespace stream9::filesystem {}

namespace stream9 {

namespace st9 = stream9;
namespace fs = st9::filesystem;

} // namespace stream9

#endif // STREAM9_FILESYSTEM_NAMESPACE_HPP
