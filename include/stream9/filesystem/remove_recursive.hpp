#ifndef STREAM9_FILESYSTEM_REMOVE_RECURSIVE_HPP
#define STREAM9_FILESYSTEM_REMOVE_RECURSIVE_HPP

#include "error.hpp"

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/fd.hpp>

namespace stream9::filesystem {

void
remove_recursive(cstring_ptr const& pathname);

void
remove_recursive(lx::fd_ref dir, cstring_ptr const& pathname);

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_REMOVE_RECURSIVE_HPP
