#ifndef STREAM9_FILESYSTEM_LOAD_STRING_HPP
#define STREAM9_FILESYSTEM_LOAD_STRING_HPP

#include "error.hpp"
#include "namespace.hpp"

#include <stream9/cstring_ptr.hpp>
#include <stream9/safe_integer.hpp>
#include <stream9/string.hpp>

namespace stream9::filesystem {

/**
 * Load content of file pointed by path into a string
 */
string
load_string(cstring_ptr const& path,
            safe_integer<int64_t, 0> size_limit = 8 * 1024 * 1024);

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_LOAD_STRING_HPP
