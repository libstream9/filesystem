#ifndef STREAM9_FILESYSTEM_OSTREAM_IPP
#define STREAM9_FILESYSTEM_OSTREAM_IPP

#include <stream9/linux/dup.hpp>
#include <stream9/linux/error.hpp>

namespace stream9 {

/*
 * basic_ofstream
 */
template<typename C, typename T>
basic_ofstream<C, T>::
basic_ofstream()
{
    try {
        this->init(&m_rdbuf);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
basic_ofstream<C, T>::
basic_ofstream(cstring_ptr filename)
{
    try {
        this->init(&m_rdbuf);
        open(filename);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
basic_ofstream<C, T>::
basic_ofstream(cstring_ptr filename, openmode mode)
{
    try {
        this->init(&m_rdbuf);
        open(filename, mode);
    }
catch (...) {
    rethrow_error();
}
}

template<typename C, typename T>
basic_ofstream<C, T>::
basic_ofstream(lx::fd_ref fd)
    try : m_rdbuf { lx::dup(fd), this->out }
{
    this->init(&m_rdbuf);
    if (!m_rdbuf.is_open()) {
        this->setstate(this->failbit);
    }
}
catch (...) {
    rethrow_error();
}

template<typename C, typename T>
basic_ofstream<C, T>::
basic_ofstream(lx::fd&& fd)
    try : m_rdbuf { fd, this->out }
{
    this->init(&m_rdbuf);
    if (m_rdbuf.is_open()) {
        fd.release();
    }
    else {
        this->setstate(this->failbit);
    }
}
catch (...) {
    rethrow_error();
}

template<typename C, typename T>
basic_ofstream<C, T>::
basic_ofstream(basic_ofstream&& o) noexcept
    : base_type { std::move(o) }
    , m_rdbuf { std::move(o.m_rdbuf) }
{
    try {
        this->init(&m_rdbuf);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
basic_ofstream<C, T>& basic_ofstream<C, T>::
operator=(basic_ofstream&& o) noexcept
{
    base_type::operator=(std::move(o));
    m_rdbuf = std::move(o.m_rdbuf);

    return *this;
}

template<typename C, typename T>
void basic_ofstream<C, T>::
swap(basic_ofstream& o) noexcept
{
    base_type::swap(o);
    m_rdbuf.swap(o.m_rdbuf);
}

template<typename C, typename T>
lx::fd_ref basic_ofstream<C, T>::
fd() noexcept
{
    return m_rdbuf.fd();
}

template<typename C, typename T>
std::basic_filebuf<C, T>* basic_ofstream<C, T>::
rdbuf() const noexcept
{
    return &m_rdbuf;
}

template<typename C, typename T>
bool basic_ofstream<C, T>::
is_open() const noexcept
{
    return m_rdbuf.is_open();
}

template<typename C, typename T>
void basic_ofstream<C, T>::
open(cstring_ptr filename)
{
    try {
        if (m_rdbuf.open(filename.c_str(), this->out) == nullptr) {
            this->setstate(this->failbit);
        }
        else {
            this->init(&m_rdbuf);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
void basic_ofstream<C, T>::
open(cstring_ptr filename, openmode mode)
{
    try {
        if (m_rdbuf.open(filename, this->out | mode) == nullptr) {
            this->setstate(this->failbit);
        }
        else {
            this->init(&m_rdbuf);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
void basic_ofstream<C, T>::
close()
{
    try {
        if (m_rdbuf.close() == nullptr) {
            this->setstate(this->failbit);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
void
swap(basic_ofstream<C, T>& x, basic_ofstream<C, T>& y) noexcept
{
    x.swap(y);
}

/*
 * basic_ifstream
 */
template<typename C, typename T>
basic_ifstream<C, T>::
basic_ifstream()
{
    try {
        this->init(&m_rdbuf);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
basic_ifstream<C, T>::
basic_ifstream(cstring_ptr filename)
{
    try {
        this->init(&m_rdbuf);
        open(filename);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
basic_ifstream<C, T>::
basic_ifstream(cstring_ptr filename, openmode mode)
{
    try {
        this->init(&m_rdbuf);
        open(filename, mode);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
basic_ifstream<C, T>::
basic_ifstream(lx::fd&& fd)
    try : m_rdbuf { fd, this->in }
{
    this->init(&m_rdbuf);
    if (m_rdbuf.is_open()) {
        fd.release();
    }
    else {
        this->setstate(this->failbit);
    }
}
catch (...) {
    rethrow_error();
}

template<typename C, typename T>
basic_ifstream<C, T>::
basic_ifstream(basic_ifstream&& o) noexcept
    : base_type { std::move(o) }
    , m_rdbuf { std::move(o.m_rdbuf) }
{
    try {
        this->init(&m_rdbuf);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
basic_ifstream<C, T>& basic_ifstream<C, T>::
operator=(basic_ifstream&& o) noexcept
{
    base_type::operator=(std::move(o));
    m_rdbuf = std::move(o.m_rdbuf);

    return *this;
}

template<typename C, typename T>
void basic_ifstream<C, T>::
swap(basic_ifstream& o) noexcept
{
    base_type::swap(o);
    m_rdbuf.swap(o.m_rdbuf);
}

template<typename C, typename T>
lx::fd_ref basic_ifstream<C, T>::
fd() noexcept
{
    return m_rdbuf.fd();
}

template<typename C, typename T>
std::basic_filebuf<C, T>* basic_ifstream<C, T>::
rdbuf() const noexcept
{
    return &m_rdbuf;
}

template<typename C, typename T>
bool basic_ifstream<C, T>::
is_open() const noexcept
{
    return m_rdbuf.is_open();
}

template<typename C, typename T>
void basic_ifstream<C, T>::
open(cstring_ptr filename)
{
    try {
        if (m_rdbuf.open(filename, this->in) == nullptr) {
            this->setstate(this->failbit);
        }
        else {
            this->init(&m_rdbuf);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
void basic_ifstream<C, T>::
open(cstring_ptr filename, openmode mode)
{
    try {
        if (m_rdbuf.open(filename, this->in | mode) == nullptr) {
            this->setstate(this->failbit);
        }
        else {
            this->init(&m_rdbuf);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
void basic_ifstream<C, T>::
close()
{
    try {
        if (m_rdbuf.close() == nullptr) {
            this->setstate(this->failbit);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
void
swap(basic_ifstream<C, T>& x, basic_ifstream<C, T>& y) noexcept
{
    x.swap(y);
}

/*
 * basic_fstream
 */
template<typename C, typename T>
basic_fstream<C, T>::
basic_fstream()
{
    try {
        this->init(&m_rdbuf);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
basic_fstream<C, T>::
basic_fstream(cstring_ptr filename)
{
    try {
        this->init(&m_rdbuf);
        open(filename);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
basic_fstream<C, T>::
basic_fstream(cstring_ptr filename, openmode mode)
{
    try {
        this->init(&m_rdbuf);
        open(filename, mode);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
basic_fstream<C, T>::
basic_fstream(lx::fd&& fd)
    try : m_rdbuf { fd, this->in | this->out }
{
    this->init(&m_rdbuf);
    if (m_rdbuf.is_open()) {
        fd.release();
    }
    else {
        this->setstate(this->failbit);
    }
}
catch (...) {
    rethrow_error();
}

template<typename C, typename T>
basic_fstream<C, T>::
basic_fstream(basic_fstream&& o) noexcept
    : base_type { std::move(o) }
    , m_rdbuf { std::move(o.m_rdbuf) }
{
    try {
        this->init(&m_rdbuf);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
basic_fstream<C, T>& basic_fstream<C, T>::
operator=(basic_fstream&& o) noexcept
{
    base_type::operator=(std::move(o));
    m_rdbuf = std::move(o.m_rdbuf);

    return *this;
}

template<typename C, typename T>
void basic_fstream<C, T>::
swap(basic_fstream& o) noexcept
{
    base_type::swap(o);
    m_rdbuf.swap(o.m_rdbuf);
}

template<typename C, typename T>
lx::fd_ref basic_fstream<C, T>::
fd() noexcept
{
    return m_rdbuf.fd();
}

template<typename C, typename T>
std::basic_filebuf<C, T>* basic_fstream<C, T>::
rdbuf() const noexcept
{
    return &m_rdbuf;
}

template<typename C, typename T>
bool basic_fstream<C, T>::
is_open() const noexcept
{
    return m_rdbuf.is_open();
}

template<typename C, typename T>
void basic_fstream<C, T>::
open(cstring_ptr filename)
{
    try {
        if (m_rdbuf.open(filename, this->in | this->out) == nullptr) {
            this->setstate(this->failbit);
        }
        else {
            this->init(&m_rdbuf);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
void basic_fstream<C, T>::
open(cstring_ptr filename, openmode mode)
{
    try {
        if (m_rdbuf.open(filename, this->in | this->out | mode) == nullptr) {
            this->setstate(this->failbit);
        }
        else {
            this->init(&m_rdbuf);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
void basic_fstream<C, T>::
close()
{
    try {
        if (m_rdbuf.close() == nullptr) {
            this->setstate(this->failbit);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename C, typename T>
void
swap(basic_fstream<C, T>& x, basic_fstream<C, T>& y) noexcept
{
    x.swap(y);
}

} // namespace stream9

namespace stream9::linux {

template<typename T>
ofstream
operator<<(fd_ref fd, T const& v)
{
    try {
        ofstream os { fd };
        os << v;
        return os;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::linux

#endif // STREAM9_FILESYSTEM_OSTREAM_IPP
