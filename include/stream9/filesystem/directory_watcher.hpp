#ifndef STREAM9_FILESYSTEM_DIRECTORY_WATCHER_HPP
#define STREAM9_FILESYSTEM_DIRECTORY_WATCHER_HPP

#include <stream9/linux/fd.hpp>
#include <stream9/node.hpp>
#include <stream9/string_view.hpp>

namespace stream9::filesystem {

class directory_watch;

class directory_watcher
{
public:
    struct event_handler;
    struct watch;

public:
    // essentials
    directory_watcher();

    ~directory_watcher() noexcept;

    directory_watcher(directory_watcher const&) = delete;
    directory_watcher& operator=(directory_watcher const&) = delete;
    directory_watcher(directory_watcher&&) = delete;
    directory_watcher& operator=(directory_watcher&&) = delete;

    // accessor
    lx::fd_ref fd() const noexcept;

    // modifier
    void add_watch(string_view dirpath, event_handler&);

    void remove_watch(string_view dirpath) noexcept;

    // command
    void process_events();

public:
    struct impl;

    class event_handler
    {
    public:
        event_handler() = default;
        virtual ~event_handler() noexcept;

        event_handler(event_handler const&) = delete;
        event_handler& operator=(event_handler const&) = delete;
        event_handler(event_handler&&) = delete;
        event_handler& operator=(event_handler&&) = delete;

        virtual void file_modified(string_view/*dirpath*/, string_view/*fname*/) {}
        virtual void file_deleted(string_view/*dirpath*/, string_view/*fname*/) {}
        virtual void directory_disappeared(string_view/*dirpath*/) {}

    private:
        friend class impl;
        impl* m_impl = nullptr;
    };

private:
    node<impl> m_impl;
};

class directory_watcher::watch
{
public:
    struct impl;

public:
    watch(node<impl>) noexcept;
    ~watch() noexcept;

private:
    node<impl> m_impl;
};

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_DIRECTORY_WATCHER_HPP
