#ifndef STREAM9_FILESYSTEM_TEMPORARY_FILE_HPP
#define STREAM9_FILESYSTEM_TEMPORARY_FILE_HPP

#include "error.hpp"
#include "fstream.hpp"
#include "namespace.hpp"

#include <compare>
#include <ostream>

#include <stream9/cstring_view.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/string.hpp>

namespace stream9::filesystem {

class temporary_file
{
public:
    /**
     * Create temporary file
     *
     * A file will be uniquely named with prefix and random characters
     * (see mkstemp(3)).
     * If prefix is empty or relative, temorary file will be created under
     * system default temporary directory
     *
     * @param prefix prefix path of temporary file
     */
    temporary_file(string_view prefix = "");

    temporary_file(temporary_file&&) noexcept;
    temporary_file& operator=(temporary_file&&) noexcept;

    /**
     * On destruction, temporary file will be closed and removed from
     * filesystem.
     */
    ~temporary_file() noexcept;

    // accessor

    /**
     * @return file descriptor of temporary file
     */
    lx::fd_ref fd() const noexcept { return m_fd; }
    lx::fd& fd() noexcept { return m_fd; }

    /**
     * @return absolute path to temporary file
     */
    cstring_view path() const noexcept { return m_path; }

    bool auto_delete() const noexcept { return m_auto_delete; }

    // modifier
    void set_auto_delete(bool) noexcept;

    // command
    void close();

    void rename(string_view newpath);

    // ordering
    bool operator==(temporary_file const&) const noexcept = default;
    std::strong_ordering operator<=>(temporary_file const&) const noexcept;

private:
    string m_path;
    lx::fd m_fd;
    bool m_auto_delete = true;
};

inline void  temporary_file::
set_auto_delete(bool v) noexcept
{
    m_auto_delete = v;
}

inline std::strong_ordering temporary_file::
operator<=>(temporary_file const& other) const noexcept
{
    return m_path <=> other.m_path;
}

inline std::ostream&
operator<<(std::ostream& os, temporary_file const& f)
{
    return os << f.path();
}

template<typename T>
ofstream
operator<<(temporary_file& f, T const& v)
{
    try {
        ofstream os { f.fd() };
        os << v;
        return os;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_TEMPORARY_FILE_HPP
