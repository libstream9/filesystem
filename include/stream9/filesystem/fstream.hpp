#ifndef STREAM9_FILESYSTEM_FSTREAM_HPP
#define STREAM9_FILESYSTEM_FSTREAM_HPP

#include "error.hpp"
#include "namespace.hpp"

#include <istream>
#include <ostream>

#include <ext/stdio_filebuf.h>

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/fd.hpp>

namespace stream9 {

/*
 * basic_ofstream
 */
template<typename CharT,
         typename Traits = std::char_traits<CharT> >
class basic_ofstream : public std::basic_ostream<CharT, Traits>
{
    using base_type = std::basic_ostream<CharT, Traits>;
public:
    using openmode = std::ios_base::openmode;

public:
    // essentials
    basic_ofstream();

    explicit basic_ofstream(cstring_ptr filename);
    basic_ofstream(cstring_ptr filename, openmode);

    basic_ofstream(lx::fd_ref);
    basic_ofstream(lx::fd&&);

    basic_ofstream(basic_ofstream const&) = delete;
    basic_ofstream& operator=(basic_ofstream const&) = delete;

    basic_ofstream(basic_ofstream&&) noexcept;
    basic_ofstream& operator=(basic_ofstream&&) noexcept;

    void swap(basic_ofstream&) noexcept;

    // accessor
    lx::fd_ref fd() noexcept;

    std::basic_filebuf<CharT, Traits>* rdbuf() const noexcept;

    // query
    bool is_open() const noexcept;

    // command
    void open(cstring_ptr filename);
    void open(cstring_ptr filename, openmode);

    void close();

private:
    __gnu_cxx::stdio_filebuf<CharT, Traits> m_rdbuf;
};

using ofstream = basic_ofstream<char>;

template<typename C, typename T>
void swap(basic_ofstream<C, T>&, basic_ofstream<C, T>&) noexcept;

/*
 * basic_ifstream
 */
template<typename CharT,
         typename Traits = std::char_traits<CharT> >
class basic_ifstream : public std::basic_istream<CharT, Traits>
{
    using base_type = std::basic_istream<CharT, Traits>;
public:
    using openmode = std::ios_base::openmode;

public:
    // essentials
    basic_ifstream();

    explicit basic_ifstream(cstring_ptr filename);
    basic_ifstream(cstring_ptr filename, openmode);

    basic_ifstream(lx::fd&&);

    basic_ifstream(basic_ifstream const&) = delete;
    basic_ifstream& operator=(basic_ifstream const&) = delete;

    basic_ifstream(basic_ifstream&&) noexcept;
    basic_ifstream& operator=(basic_ifstream&&) noexcept;

    void swap(basic_ifstream&) noexcept;

    // accessor
    lx::fd_ref fd() noexcept;

    std::basic_filebuf<CharT, Traits>* rdbuf() const noexcept;

    // query
    bool is_open() const noexcept;

    // command
    void open(cstring_ptr filename);
    void open(cstring_ptr filename, openmode);

    void close();

private:
    __gnu_cxx::stdio_filebuf<CharT, Traits> m_rdbuf;
};

using ifstream = basic_ifstream<char>;

template<typename C, typename T>
void swap(basic_ifstream<C, T>&, basic_ifstream<C, T>&) noexcept;

/*
 * basic_fstream
 */
template<typename CharT,
         typename Traits = std::char_traits<CharT> >
class basic_fstream : public std::basic_iostream<CharT, Traits>
{
    using base_type = std::basic_iostream<CharT, Traits>;
public:
    using openmode = std::ios_base::openmode;

public:
    // essentials
    basic_fstream();

    explicit basic_fstream(cstring_ptr filename);
    basic_fstream(cstring_ptr filename, openmode);

    basic_fstream(lx::fd&&);

    basic_fstream(basic_fstream const&) = delete;
    basic_fstream& operator=(basic_fstream const&) = delete;

    basic_fstream(basic_fstream&&) noexcept;
    basic_fstream& operator=(basic_fstream&&) noexcept;

    void swap(basic_fstream&) noexcept;

    // accessor
    lx::fd_ref fd() noexcept;

    std::basic_filebuf<CharT, Traits>* rdbuf() const noexcept;

    // query
    bool is_open() const noexcept;

    // command
    void open(cstring_ptr filename);
    void open(cstring_ptr filename, openmode);

    void close();

private:
    __gnu_cxx::stdio_filebuf<CharT, Traits> m_rdbuf;
};

using fstream = basic_fstream<char>;

template<typename C, typename T>
void swap(basic_fstream<C, T>&, basic_fstream<C, T>&) noexcept;

} // namespace stream9

namespace stream9::linux {

template<typename T>
ofstream
operator<<(fd_ref fd, T const& v);

} // namespace stream9::linux

#endif // STREAM9_FILESYSTEM_FSTREAM_HPP

#include "fstream.ipp"
