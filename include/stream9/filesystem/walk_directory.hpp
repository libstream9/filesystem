#ifndef STREAM9_FILESYSTEM_WALK_DIRECTORY_HPP
#define STREAM9_FILESYSTEM_WALK_DIRECTORY_HPP

#include <stream9/linux/directory.hpp>
#include <stream9/string_view.hpp>

namespace stream9::filesystem {

template<typename Fn>
void
walk_directory(lx::directory& dir,
               Fn callback,
               int depth = 1,
               int max_depth = 10)
    requires std::invocable<Fn, lx::directory&, struct ::dirent&>;

template<typename Fn>
void
walk_directory(string_view dirpath,
               Fn callback,
               int depth = 1,
               int max_depth = 10)
    requires std::invocable<Fn, lx::directory&, struct ::dirent&>;

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_WALK_DIRECTORY_HPP

#include "walk_directory.ipp"
