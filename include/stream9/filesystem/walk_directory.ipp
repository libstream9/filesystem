#include "error.hpp"
#include "namespace.hpp"

#include <stream9/log.hpp>
#include <stream9/path.hpp>

namespace stream9::filesystem {

template<typename Fn>
void
walk_directory(lx::directory& dir,
               Fn callback,
               int depth/* = 1*/,
               int max_depth/* = 10*/)
    requires std::invocable<Fn, lx::directory&, struct ::dirent&>
{
    using path::operator/;

    try {
        for (auto& e: dir) {
            auto n = lx::name(e);
            if (n == "." || n == "..") continue;

            if (lx::is_directory(e, dir.fd())) {
                auto dirpath = dir.path() / n;

                if (depth > max_depth) {
                    log::err() << "directory is too deep:" << dirpath;
                    continue;
                }

                auto o_d = lx::directory::open(dirpath);
                if (o_d) {
                    walk_directory(*o_d, callback, depth + 1);
                }
                else if (o_d.error() == lx::errc::enoent) {
                    log::dbg() << "there isn't a directory at" << dirpath;
                }
                else if (o_d.error() == lx::errc::eacces) {
                    log::dbg() << "permission denied on opening a directory" << dirpath;
                }
                else {
                    throw error {
                        "opendir()", o_d.error(), {
                            { "dirpath", dirpath }
                        }
                    };
                }
            }
            else {
                callback(dir, e);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Fn>
void
walk_directory(string_view dirpath,
               Fn callback,
               int depth/* = 1*/,
               int max_depth/* = 10*/)
    requires std::invocable<Fn, lx::directory&, struct ::dirent&>
{
    using path::operator/;

    try {
        auto o_d = lx::directory::open(dirpath);
        if (!o_d) {
            if (o_d.error() == lx::errc::enoent) {
                log::dbg() << "there isn't a directory at" << dirpath;
            }
            else if (o_d.error() == lx::errc::eacces) {
                log::dbg() << "permission denied on opening a directory" << dirpath;
            }
            else {
                throw error {
                    "opendir()", o_d.error(), {
                        { "dirpath", dirpath }
                    }
                };
            }
        }

        walk_directory(*o_d, std::move(callback), depth, max_depth);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::filesystem
