#ifndef STREAM9_FILESYSTEM_ERROR_HPP
#define STREAM9_FILESYSTEM_ERROR_HPP

#include <system_error>

#include <stream9/json.hpp>
#include <stream9/errors.hpp>

namespace stream9::filesystem {

enum class errc {
    ok = 0,
    invalid_environment_variable,
    fail_to_open_file,
    fail_to_get_status,
    not_regular_file,
    fail_to_read_file,
    fail_to_create_file,
    fail_to_create_directory,
    file_already_exists,
    fail_to_remove_file,
    fail_to_remove_directory,
};

std::error_category const& error_category() noexcept;

inline std::error_code
make_error_code(errc e) noexcept
{
    return { static_cast<int>(e), error_category() };
}

} // namespace stream9::filesystem

namespace std {

template<>
struct is_error_code_enum<stream9::filesystem::errc> : true_type {};

} // namespace std

#endif // STREAM9_FILESYSTEM_ERROR_HPP
