#include <stream9/filesystem/json.hpp>
#include <stream9/filesystem/json.ipp>

namespace stream9::json {

template void
tag_invoke(json::value_from_tag, json::value&, std::basic_istream<char>&);

template void
tag_invoke(json::value_from_tag, json::value&, std::basic_ostream<char>&);

template void
tag_invoke(json::value_from_tag, json::value&, std::basic_iostream<char>&);

} // namespace stream9::json
