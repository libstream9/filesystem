#include <stream9/filesystem/mkdir_recursive.hpp>

#include <stream9/linux/mkdir.hpp>
#include <stream9/linux/stat.hpp>
#include <stream9/path/dirname.hpp>
#include <stream9/strings/empty.hpp>

namespace stream9::filesystem {

safe_integer<int, 0>
mkdir_recursive(cstring_ptr const& path, ::mode_t mode/*= 0777*/)
{
    try {
        if (str::empty(path)) return 0;

        auto oc = lx::nothrow::mkdir(path, mode);
        if (oc) {
            return 1;
        }
        else if (oc == lx::errc::enoent) { // parent directory doesn't exist
            auto parent = path::dirname(path);
            auto n = mkdir_recursive(parent, mode);

            try {
                lx::mkdir(path, mode);
            }
            catch (...) {
                throw_error(errc::fail_to_create_directory);
            }

            return n + 1;
        }
        else if (oc == lx::errc::eexist) { // path already exists
            auto st = lx::stat(path);
            if (lx::is_directory(st)) {
                return 0;
            }
            else {
                throw_error(errc::file_already_exists);
            }
        }
        else {
            try {
                throw error { "mkdir(2)", oc.error() };
            }
            catch (...) {
                throw_error(errc::fail_to_create_directory);
            }
        }
    }
    catch (...) {
        rethrow_error({
            { "path", path }
        });
    }
}

} // namespace stream9::filesystem
