#include <stream9/filesystem/directory_watcher.hpp>

#include <stream9/bits.hpp>
#include <stream9/expand.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/linux/inotify.hpp>
#include <stream9/log.hpp>
#include <stream9/optional.hpp>
#include <stream9/ref.hpp>
#include <stream9/sorted_table.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>
#include <stream9/unique_table.hpp>

namespace stream9::filesystem {

struct directory_watcher::impl
{
    directory_watcher& m_p;
    lx::inotify m_inotify;
    unique_table<int/*wd*/, string/*dirpath*/> m_wd_dirpath;
    sorted_table<int/*wd*/, ref<event_handler>> m_wd_handler;

    static constexpr uint32_t modification_mask = IN_CLOSE_WRITE | IN_MOVED_TO;
    static constexpr uint32_t deletion_mask = IN_DELETE | IN_MOVED_FROM;
    static constexpr uint32_t disappear_mask = IN_DELETE_SELF | IN_MOVE_SELF;
    static constexpr uint32_t unmount_mask = IN_UNMOUNT;
    static constexpr uint32_t watch_mask =
                             modification_mask | deletion_mask | disappear_mask;

    impl(directory_watcher& p)
        : m_p { p }
        , m_inotify { IN_NONBLOCK }
    {}

    lx::fd_ref
    fd() const noexcept
    {
        return m_inotify.fd();
    }

    opt<string_view>
    find_dirpath(int wd) noexcept
    {
        auto i = m_wd_dirpath.find(wd);
        if (i == m_wd_dirpath.end()) {
            return {};
        }
        else {
            return i->value;
        }
    }

    opt<int>
    find_watch(string_view dirpath) noexcept
    {
        for (auto& [w, p]: m_wd_dirpath) {
            if (p == dirpath) {
                return w;
            }
        }
        return {};
    }

    auto
    find_handler(event_handler& h) noexcept
    {
        using std::ranges::find_if;

        return find_if(m_wd_handler,
            [&](auto& e) {
                return &e.value == &h;
            });
    }

    void
    add_watch(string_view dirpath, directory_watcher::event_handler& h)
    {
        auto o_w = find_watch(dirpath);
        if (!o_w) {
            st9::expand(m_wd_dirpath, 1);
            st9::expand(m_wd_handler, 1);

            auto wd = m_inotify.add_watch(dirpath, watch_mask);

            // noexcept from here

            m_wd_dirpath.emplace(wd, dirpath);
            m_wd_handler.emplace(wd, h);
        }
        else {
            m_wd_handler.emplace(*o_w, h);
        }

        h.m_impl = this;
    }

    void
    remove_watch(int wd) noexcept
    {
        m_wd_dirpath.erase(wd);
        m_wd_handler.erase(wd);
        try {
            m_inotify.rm_watch(wd);
        }
        catch (...) {}
    }

    void
    remove_watch(string_view dirpath) noexcept
    {
        auto o_w = find_watch(dirpath);
        if (o_w) {
            remove_watch(*o_w);
        }
    }

    void
    remove_watch(event_handler& h) noexcept
    {
        while (true) {
            auto i = find_handler(h);
            if (i == m_wd_handler.end()) break;

            auto wd = i->key;
            m_wd_handler.erase(i);

            auto j = m_wd_handler.find(wd);
            if (j == m_wd_handler.end()) {
                m_wd_dirpath.erase(wd);
                try {
                    m_inotify.rm_watch(wd);
                }
                catch (...) {}
            }
        }
    }

    void
    call_handlers(auto& ev, auto& dirpath, auto& fname) noexcept
    {
        using st9::bits::any_of;

        for (auto& [_, h]: m_wd_handler.equal_range(ev.wd)) {
            try {
                if (any_of(ev.mask, modification_mask)) {
                    h->file_modified(dirpath, fname);
                }
                else if (any_of(ev.mask, deletion_mask)) {
                    h->file_deleted(dirpath, fname);
                }
                else if (any_of(ev.mask, disappear_mask | unmount_mask)) {
                    h->directory_disappeared(dirpath);
                }
            }
            catch (...) {
                print_error(log::err());
            }
        }
    }

    void
    process_events()
    {
        using st9::bits::any_of;

        char buf[1024];

        while (true) {
            auto events = m_inotify.read(buf);
            if (events.empty()) break;

            for (auto& ev: events) {
                try {
                    auto o_p = find_dirpath(ev.wd);
                    if (!o_p) continue;

                    auto& dirpath = *o_p;
                    auto fname = lx::name(ev);

                    call_handlers(ev, dirpath, fname);

                    if (any_of(ev.mask, IN_IGNORED)) {
                        m_wd_dirpath.erase(ev.wd);
                        m_wd_handler.erase(ev.wd);
                    }
                }
                catch (...) {
                    print_error(log::err());
                }
            }
        }
    }

};

/*
 * class event_handler
 */
directory_watcher::event_handler::
~event_handler() noexcept
{
    if (m_impl) {
        m_impl->remove_watch(*this);
    }
}

/*
 * class directory_watcher
 */
directory_watcher::
directory_watcher()
    try : m_impl { *this }
{}
catch (...) {
    rethrow_error();
}

directory_watcher::
~directory_watcher() noexcept = default;

lx::fd_ref directory_watcher::
fd() const noexcept
{
    return m_impl->fd();
}

void directory_watcher::
add_watch(string_view dirpath, event_handler& h)
{
    try {
        m_impl->add_watch(dirpath, h);
    }
    catch (...) {
        rethrow_error();
    }
}

void directory_watcher::
remove_watch(string_view dirpath) noexcept
{
    m_impl->remove_watch(dirpath);
}

void directory_watcher::
process_events()
{
    try {
        m_impl->process_events();
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::filesystem
