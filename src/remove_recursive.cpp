#include <stream9/filesystem/remove_recursive.hpp>

#include <stream9/linux/directory.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/linux/rmdir.hpp>
#include <stream9/linux/unlink.hpp>
#include <stream9/path/concat.hpp> // operator/

namespace stream9::filesystem {

void
remove_recursive(cstring_ptr const& p)
{
    using path::operator/;

    try {
        auto oc = lx::nothrow::unlink(p);
        if (oc) return;
        else if (oc == lx::errc::eisdir) {
            lx::directory dir { p };
            for (auto const& ent: dir) {
                auto name = lx::name(ent);
                if (name == "." || name == "..") continue;
                remove_recursive(dir.fd(), name);
            }
            try {
                lx::rmdir(p);
            }
            catch (...) {
                throw error { errc::fail_to_remove_directory };
            }
        }
        else {
            try {
                throw error { "unlink(2)", lx::make_error_code(errno) };
            }
            catch (...) {
                throw_error(errc::fail_to_remove_file);
            }
        }
    }
    catch (...) {
        rethrow_error({
            { "path", p }
        });
    }
}

void
remove_recursive(lx::fd_ref dir, cstring_ptr const& filename)
{
    using path::operator/;

    try {
        auto oc = lx::nothrow::unlink(dir, filename);
        if (oc) return;
        else if (oc == lx::errc::eisdir) {
            lx::directory dir2 { dir, filename };
            for (auto const& ent: dir2) {
                auto name = lx::name(ent);
                if (name == "." || name == "..") continue;
                remove_recursive(dir2.fd(), name);
            }
            try {
                lx::rmdir(dir, filename);
            }
            catch (...) {
                throw error { errc::fail_to_remove_directory };
            }
        }
        else {
            try {
                throw error { "unlink(2)", lx::make_error_code(errno) };
            }
            catch (...) {
                throw_error(errc::fail_to_remove_file);
            }
        }
    }
    catch (...) {
        rethrow_error({
            { "dirfd", dir },
            { "pathname", filename },
        });
    }
}

} // namespace stream9::filesystem
