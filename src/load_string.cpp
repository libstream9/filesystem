#include <stream9/filesystem/load_string.hpp>

#include <stream9/filesystem/error.hpp>

#include <stream9/linux/open.hpp>
#include <stream9/linux/read_n.hpp>
#include <stream9/linux/stat.hpp>

namespace stream9::filesystem {

static lx::fd
open_file_for_read(cstring_ptr const& path)
{
    try {
        return lx::open(path, O_RDONLY);
    }
    catch (...) {
        throw_error(errc::fail_to_open_file);
    }
}

static auto
get_status(lx::fd_ref fd)
{
    try {
        return lx::fstat(fd);
    }
    catch (...) {
        throw_error(errc::fail_to_get_status);
    }
}

string
load_string(cstring_ptr const& path,
            safe_integer<int64_t, 0> size_limit)
{
    try {
        auto fd = open_file_for_read(path);
        auto st = get_status(fd);

        if (!lx::is_regular_file(st)) {
            throw error {
                errc::not_regular_file, {
                    { "stat", json::value_from(st) }
                }
            };
        }

        safe_integer<int64_t, 0> file_size = st.st_size;
        auto size = file_size < size_limit ? file_size : size_limit;

        string s;
        s.resize(size);

        auto oc = lx::read_n(fd, s);
        if (!oc) {
            try {
                throw error { "read(2)", oc.ec };
            }
            catch (...) {
                throw_error(errc::fail_to_read_file);
            }
        }
        if (*oc != size) {
            s.resize(*oc);
        }

        return s;
    }
    catch (...) {
        rethrow_error({
            { "path", path },
            { "size limit", size_limit },
        });
    }
}

} // namespace stream9::filesystem
