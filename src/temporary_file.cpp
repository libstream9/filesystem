#include <stream9/filesystem/temporary_file.hpp>

#include <stdlib.h>

#include <stream9/linux/error.hpp>
#include <stream9/linux/remove.hpp>
#include <stream9/linux/rename.hpp>
#include <stream9/log.hpp>
#include <stream9/path/absolute.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/path/relative.hpp>
#include <stream9/path/temporary_directory.hpp>
#include <stream9/string.hpp>
#include <stream9/strings/concat.hpp>
#include <stream9/strings/empty.hpp>

namespace stream9::filesystem {

static string
resolve_path(string_view prefix)
{
    using path::operator/;
    using str::operator+;

    try {
        if (str::empty(prefix)) {
            return path::temporary_directory() / "XXXXXX";
        }
        else if (path::is_relative(prefix)) {
            return path::temporary_directory() / prefix + "XXXXXX";
        }
        else {
            return prefix + "XXXXXX";
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static lx::fd
open_file(string& path)
{
    auto fd = ::mkstemp(path.data());
    if (fd == -1) {
        try {
            throw error { "mkstemp(3)", lx::make_error_code(errno), {
                { "path", path }
            } };
        }
        catch (...) {
            throw_error(errc::fail_to_create_file);
        }
    }
    return lx::fd(fd);
}

temporary_file::
temporary_file(string_view prefix/*= ""*/)
    try : m_path { resolve_path(prefix) }
        , m_fd { open_file(m_path) }
{
    if (path::is_relative(m_path)) {
        m_path = path::to_absolute(m_path);
    }
}
catch (...) {
    rethrow_error({
        { "prefix", prefix }
    });
}

temporary_file::
temporary_file(temporary_file&& o) noexcept
    : m_path { std::move(o.m_path) }
    , m_fd { std::move(o.m_fd) }
    , m_auto_delete { o.m_auto_delete }
{
    o.m_auto_delete = false;
}

temporary_file& temporary_file::
operator=(temporary_file&& o) noexcept
{
    temporary_file::~temporary_file();

    m_fd = std::move(o.m_fd);
    m_path = std::move(o.m_path);
    m_auto_delete = o.m_auto_delete;

    o.m_auto_delete = false;

    return *this;
}

temporary_file::
~temporary_file() noexcept
{
    try {
        if (m_auto_delete) {
            lx::remove(m_path);
        }
    }
    catch (...) {
        log::warn() << "fail to remove temporary file";
        print_error(log::warn());
    }
}

void temporary_file::
close()
{
    try {
        m_fd.close();
    }
    catch (...) {
        rethrow_error();
    }
}

void temporary_file::
rename(string_view newpath)
{
    try {
        lx::rename(path(), newpath);
        m_auto_delete = false;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::filesystem
