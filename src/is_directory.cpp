#include <stream9/filesystem/is_directory.hpp>

#include <stream9/linux/stat.hpp>

namespace stream9::filesystem {

bool
is_directory(cstring_ptr path) noexcept
{
    auto o_st = lx::nothrow::stat(path);
    return o_st && lx::is_directory(*o_st);
}

} // namespace stream9::filesystem
