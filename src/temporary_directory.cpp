#include <stream9/filesystem/temporary_directory.hpp>

#include <string>
#include <system_error>

#include <stdlib.h>

#include <stream9/filesystem/mkdir_recursive.hpp>
#include <stream9/filesystem/remove_recursive.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/rmdir.hpp>
#include <stream9/log.hpp>
#include <stream9/path/concat.hpp>
#include <stream9/path/dirname.hpp>
#include <stream9/path/relative.hpp>
#include <stream9/path/temporary_directory.hpp>
#include <stream9/strings/concat.hpp> // operator+

namespace stream9::filesystem {

static string
resolve_path(opt<cstring_ptr> const& prefix)
{
    using path::operator/;
    using str::operator+;

    if (!prefix) {
        return path::temporary_directory() / "XXXXXX";
    }
    else if (path::is_relative(*prefix)) {
        return path::temporary_directory() / *prefix + "XXXXXX";
    }
    else {
        return *prefix + "XXXXXX";
    }
}

temporary_directory::
temporary_directory(opt<cstring_ptr> const& prefix/*= {}*/)
try {
    m_path = resolve_path(prefix);

    m_delete_depth = mkdir_recursive(path::dirname(m_path));

    if (::mkdtemp(m_path.data()) == nullptr) {
        throw error {
            "mkdtemp(3)",
            lx::make_error_code(errno),
        };
    }
}
catch (...) {
    json::object cxt;
    if (prefix) {
        cxt["prefix"] = *prefix;
    }

    throw_error(errc::fail_to_create_directory, std::move(cxt));
}

temporary_directory::
~temporary_directory() noexcept
{
    using stream9::path::dirname;

    if (!m_auto_delete) return;

    try {
        remove_recursive(m_path);

        if (m_delete_depth > 0) {
            string_view p = m_path;
            for (size_t i = 0; i < m_delete_depth; ++i) {
                p = dirname(p);
                try {
                    lx::rmdir(p);
                }
                catch (error const& e) {
                    if (e.why() == lx::errc::enotempty) {
                        break;
                    }
                    throw;
                }
            }
        }
    }
    catch (...) {
        log::warn() << "fail to delete temporary directory: ";
        print_error(log::warn());
    }
}

} // namespace stream9::filesystem
