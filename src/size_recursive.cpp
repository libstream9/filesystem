#include <stream9/filesystem/size_recursive.hpp>

#include <stream9/linux/stat.hpp>
#include <stream9/linux/directory.hpp>

namespace stream9::filesystem {

static opt<lx::directory>
open_directory(cstring_ptr const& path)
{
    try {
        return lx::directory(path);
    }
    catch (...) {
        return {};
    }
}

static opt<lx::directory>
open_directory(lx::fd_ref dirfd, cstring_ptr const& path)
{
    try {
        return lx::directory(dirfd, path);
    }
    catch (...) {
        return {};
    }
}

safe_integer<intmax_t, 0>
size_recursive(cstring_ptr const& pathname)
{
    try {
        safe_integer<intmax_t, 0> sz = 0;

        auto o_st = lx::nothrow::fstatat(AT_FDCWD, pathname, AT_SYMLINK_NOFOLLOW);
        if (o_st) {
            sz = o_st->st_size;

            if (lx::is_directory(*o_st)) {
                auto o_dir = open_directory(pathname);
                if (o_dir) {
                    for (auto const& ent: *o_dir) {
                        auto name = lx::name(ent);
                        if (name == "." || name == "..") continue;

                        sz += size_recursive(o_dir->fd(), name);
                    }
                }
            }
        }

        return sz;
    }
    catch (...) {
        rethrow_error({
            { "pathname", pathname },
        });
    }
}

safe_integer<std::intmax_t, 0>
size_recursive(lx::fd_ref dirfd,
               cstring_ptr const& pathname)
{
    try {
        safe_integer<intmax_t, 0> sz = 0;

        auto o_st = lx::nothrow::fstatat(dirfd, pathname, AT_SYMLINK_NOFOLLOW);
        if (o_st) {
            sz = o_st->st_size;

            if (lx::is_directory(*o_st)) {
                auto o_dir = open_directory(dirfd, pathname);
                if (o_dir) {
                    for (auto const& ent: *o_dir) {
                        auto name = lx::name(ent);
                        if (name == "." || name == "..") continue;

                        sz += size_recursive(o_dir->fd(), name);
                    }
                }
            }
        }

        return sz;
    }
    catch (...) {
        rethrow_error({
            { "dir", dirfd },
            { "pathname", pathname },
        });
    }
}

} // namespace stream9::filesystem
