#include <stream9/filesystem/exists.hpp>

#include <stream9/linux/error.hpp>
#include <stream9/linux/stat.hpp>

namespace stream9::filesystem {

bool
exists(cstring_ptr const& p)
{
    try {
        auto o_st = lx::nothrow::stat(p);
        if (o_st) {
            return true;
        }
        else if (o_st == lx::enoent) {
            return false;
        }
        else {
            throw error {
                o_st.error(), {
                    { "p", p }
                }
            };
        }
    }
    catch (...) {
        rethrow_error({ { "path", p } });
    }
}

} // namespace stream9::filesystem
