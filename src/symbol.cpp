#include <stream9/filesystem/symbol.hpp>

#include "namespace.hpp"

#include <boost/container/small_vector.hpp>

#include <stream9/strings/join.hpp>
#include <stream9/strings/stream.hpp>

namespace stream9::filesystem {

std::string
iostate_to_symbol(std::ios_base::iostate const s)
{
    using str::operator<<;
    std::string result;
    bool first = true;

    auto append = [&](auto bit, auto&& symbol) {
        if ((s & bit) == bit) {
            if (!first) {
                result << " | ";
            }
            result << symbol;
            first = false;
        }
    };

    if (s == std::ios_base::goodbit) {
        result << "goodbit";
        first = false;
    }

    append(std::ios_base::badbit, "badbit");
    append(std::ios_base::failbit, "failbit");
    append(std::ios_base::eofbit, "eofbit");

    return result;
}

std::string
fmtflags_to_symbol(std::ios_base::fmtflags const f)
{
    using B = std::ios_base;

    boost::container::small_vector<char const*, 10> symbols;

    if (f & B::dec) symbols.push_back("dec");
    if (f & B::oct) symbols.push_back("oct");
    if (f & B::hex) symbols.push_back("hex");

    if (f & B::left) symbols.push_back("left");
    if (f & B::right) symbols.push_back("right");
    if (f & B::internal) symbols.push_back("internal");

    if (f & B::scientific) symbols.push_back("scientific");
    if (f & B::fixed) symbols.push_back("fixed");

    if (f & B::boolalpha) symbols.push_back("boolalpha");
    if (f & B::showbase) symbols.push_back("showbase");
    if (f & B::showpoint) symbols.push_back("showpoint");
    if (f & B::showpos) symbols.push_back("showpos");
    if (f & B::skipws) symbols.push_back("skipws");
    if (f & B::unitbuf) symbols.push_back("unitbuf");
    if (f & B::uppercase) symbols.push_back("uppercase");

    return str::join(symbols, " | ");
}


} // namespace stream9::filesystem
