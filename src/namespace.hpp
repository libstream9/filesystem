#ifndef STREAM9_FILESYSTEM_SRC_NAMESPACE_HPP
#define STREAM9_FILESYSTEM_SRC_NAMESPACE_HPP

namespace stream9::strings {}

namespace stream9::filesystem {

namespace str { using namespace stream9::strings; }

} // namespace stream9::filesystem

#endif // STREAM9_FILESYSTEM_SRC_NAMESPACE_HPP
