#include <stream9/filesystem/error.hpp>

namespace stream9::filesystem {

std::error_category const&
error_category() noexcept
{
    static struct impl : std::error_category
    {
        char const* name() const noexcept { return "stream9::filesystem"; }

        std::string message(int ec) const
        {
            switch (static_cast<errc>(ec)) {
                using enum errc;
                case ok:
                    return "ok";
                case invalid_environment_variable:
                    return "invalid environment variable";
                case fail_to_open_file:
                    return "fail to open file";
                case fail_to_get_status:
                    return "fail to get status";
                case not_regular_file:
                    return "not regular file";
                case fail_to_read_file:
                    return "fail to read file";
                case fail_to_create_file:
                    return "fail to create file";
                case fail_to_create_directory:
                    return "fail to create directory";
                case file_already_exists:
                    return "file_already_exists";
                case fail_to_remove_file:
                    return "fail to remove file";
                case fail_to_remove_directory:
                    return "fail to remove directory";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

} // namespace stream9::filesystem
